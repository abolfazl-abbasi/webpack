let head = document.getElementsByTagName('header')[0];

document.body.onscroll = function(){
  let Top = document.documentElement.scrollTop;

  if(Top > 40)head.classList.add('scroll');
  else head.classList.remove('scroll');
  
}
let btn = document.getElementsByClassName('btn');
let badge = document.getElementsByClassName('card-bad')[0];
let trash = document.getElementsByClassName('trash')[0];
let n = 0;

for(let i = 0;i <= btn.length;i++){
  btn[i].addEventListener('click' , () => {
    n++
    btn[i].innerHTML = '<i class="fas fa-check" style="color:#6d28d9;transition: all .5s;transform: rotate(1turn);"></i>'
    if(n > 0) trash.style.opacity = 1;
    setTimeout(() =>{
      btn[i].innerHTML = 'add to cart'
    },1500)
    badge.innerHTML = n
  })
  trash.addEventListener('click' , () => {
    n = 0;
    trash.style.opacity = 0;
    badge.innerHTML = 0
  })
}